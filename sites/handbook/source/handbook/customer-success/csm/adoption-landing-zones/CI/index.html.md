---
layout: handbook-page-toc
title: "CI Adoption Landing Zone"
description: "A page containing links to helpful CI resources for the CSM team and our customers"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Migrating to GitLab



GitHub to GitLab

1. Blog: https://about.gitlab.com/blog/2023/07/11/github-to-gitlab-migration-made-easy/
1. GitHub Actions to GitLab Video: https://youtu.be/0Id5oMl1Kqs  [Highspot link](https://gitlab.highspot.com/items/648a0479e3c34e922e251bcd?lfrm=shp.0)



Jenkins to GitLab

1. Docs page: 'How To' Overview: https://docs.gitlab.com/ee/ci/migration/jenkins.html
1. Jenkins Integration demo (CS-Led): https://gitlab.com/gitlab-learn-labs/webinars/cicd/jenkins-integration-demo)
1. 

## Adoption Enablement


1. Intro to CI/CD (CSM-Led): 
   - Customer-facing: https://content.gitlab.com/viewer/64cadaf3b956a3a8474c08c1
   - Internal: https://gitlab.highspot.com/items/62d048f841caa8d7a595da82?lfrm=srp.2

1.  Advanced CI/CD (CSM-Led):
    - Customer-facing: https://content.gitlab.com/viewer/64cadbda812416966124e21b
    - Internal: https://gitlab.highspot.com/items/62d16ab8ea03e5a65d81971f?lfrm=ssrp.4

1. Runner Overview: https://gitlab.highspot.com/items/64cadeb868936bb54ac9ce2f#1
    
1.  Webinars (Informational & Hands-On):
     - https://about.gitlab.com/handbook/customer-success/csm/segment/scale/webinar-calendar/
 

## Highspot pages